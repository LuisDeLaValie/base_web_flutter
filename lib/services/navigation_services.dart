import 'package:flutter/material.dart';

class NavigationServices {
  final navigationkey = GlobalKey<NavigatorState>();

  Future navigateTo(String routeName) {
    return navigationkey.currentState!.pushNamed(routeName);
  }

  void goBack(String routeName) {
    navigationkey.currentState!.pop();
  }
}

final navigationServices = NavigationServices();
