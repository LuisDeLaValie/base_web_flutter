import 'package:flutter/material.dart';

class CouterProvider with ChangeNotifier {
  bool _loader = true;

  int _couter = 15;
  CouterProvider(String labase) {
    this._couter = int.tryParse(labase) ?? 15;
  }
  int get couter => this._couter;
  set couter(int val) {
    this._couter = val;
    notifyListeners();
  }

  bool get loader => this._loader;

  set loader(bool val) {
    this._loader = val;
    notifyListeners();
  }

  void init() async {
    notifyListeners();
  }
}
