import 'package:bases_web/router/router.dart';
import 'package:bases_web/services/navigation_services.dart';
import 'package:bases_web/ui/layout/main_layout_page.dart';
import 'package:flutter/material.dart';

void main() {
  Flurorouter.configureRoutes();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Rutas Page',
      initialRoute: '/',
      // onGenerateRoute: RouterGenerater.generateRouter,
      onGenerateRoute: Flurorouter.router.generator,
      navigatorKey: navigationServices.navigationkey,
      builder: (_, child) {
        return MainLayoutPage(
          child: child!,
        );
      },
    );
  }
}
