// import 'package:bases_web/ui/views/Cointer_provider_view.dart';
// import 'package:bases_web/ui/views/Cointer_view.dart';
// import 'package:bases_web/ui/views/view_404.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';

// import 'package:flutter/foundation.dart' show kIsWeb;

// class RouterGenerater {
//   static Route<dynamic> generateRouter(RouteSettings settings) {
//     switch (settings.name) {
//       case '/stateful':
//         return _pageRoute(CointerView(), '/stateful');
//       case '/provider':
//         return _pageRoute(CointerProviderView(), '/provider');
//       default:
//         return _pageRoute(View404(), '/404');
//     }
//   }

//   static PageRoute _pageRoute(Widget child, String roterName) {
//     return PageRouteBuilder(
//       settings: RouteSettings(name: roterName),
//       pageBuilder: (_, __, ___) => child,
//       transitionDuration: Duration(milliseconds: 200),
//       transitionsBuilder: (_, animete, __, ___) => (kIsWeb)
//           ? FadeTransition(
//               opacity: animete,
//               child: child,
//             )
//           : CupertinoPageTransition(
//               primaryRouteAnimation: animete,
//               secondaryRouteAnimation: __,
//               child: child,
//               linearTransition: true,
//             ),
//     );
//   }
// }
