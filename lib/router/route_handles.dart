import 'package:bases_web/ui/views/Cointer_provider_view.dart';
import 'package:bases_web/ui/views/Cointer_view.dart';
import 'package:bases_web/ui/views/view_404.dart';
import 'package:fluro/fluro.dart';

final  Handler counterHandlrer = Handler(
    handlerFunc: (context, params) {
      return CointerView(
        labase: params['base']?[0] ?? '5',
      );
    },
  );
  final  Handler counterProviderHandlrer = Handler(
    handlerFunc: (context, params) {
      return CointerProviderView(
        labase: params['q']?[0] ?? '10',
      );
    },
  );
  final  Handler pageNotFound = Handler(
    handlerFunc: (context, params) => View404(),
  );