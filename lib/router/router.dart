import 'package:bases_web/router/route_handles.dart';
import 'package:fluro/fluro.dart';

class Flurorouter {
  static final FluroRouter router = new FluroRouter();

  static void configureRoutes() {
    router.define('/',
        handler: counterHandlrer, transitionType: TransitionType.fadeIn);
    router.define('/stateful',
        handler: counterHandlrer, transitionType: TransitionType.fadeIn);
    router.define('/stateful/:base',
        handler: counterHandlrer, transitionType: TransitionType.fadeIn);
    router.define('/provider',
        handler: counterProviderHandlrer,
        transitionType: TransitionType.fadeIn);

    router.notFoundHandler = pageNotFound;
  }
}
