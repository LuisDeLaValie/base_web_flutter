import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:bases_web/providers/Couter_provider.dart';
import 'package:bases_web/ui/shared/Custom_flat_button.dart';

class CointerProviderView extends StatelessWidget {
  final String labase;
  const CointerProviderView({
    Key? key,
    required this.labase,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => CouterProvider(this.labase),
      child: _PageProvider(),
    );
  }
}

class _PageProvider extends StatelessWidget {
  const _PageProvider({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final pro = Provider.of<CouterProvider>(context);
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          "Provider counter",
          style: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.bold,
          ),
        ),
        FittedBox(
          fit: BoxFit.contain,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Text(
              "Coutenr: ${pro.couter}",
              style: TextStyle(
                fontSize: 80,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CustaomFlatButtom(
              onPressed: () => pro.couter++,
              text: 'increment',
            ),
            CustaomFlatButtom(
              onPressed: () => pro.couter--,
              text: 'Decrementar',
            ),
          ],
        ),
      ],
    );
  }
}
