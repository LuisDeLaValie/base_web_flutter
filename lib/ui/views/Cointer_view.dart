import 'package:flutter/material.dart';

import 'package:bases_web/ui/shared/Custom_flat_button.dart';

class CointerView extends StatefulWidget {
  final String labase;
  const CointerView({
    Key? key,
    required this.labase,
  }) : super(key: key);

  @override
  _CointerViewState createState() => _CointerViewState();
}

class _CointerViewState extends State<CointerView> {
  int couter = 15;

  @override
  void initState() {
    couter = int.tryParse(widget.labase) ?? 15;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          "Statefull counter",
          style: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.bold,
          ),
        ),
        FittedBox(
          fit: BoxFit.contain,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Text(
              "Coutenr: $couter",
              style: TextStyle(
                fontSize: 80,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CustaomFlatButtom(
              onPressed: () {
                setState(() {
                  couter++;
                });
              },
              text: 'increment',
            ),
            CustaomFlatButtom(
              onPressed: () {
                setState(() {
                  couter--;
                });
              },
              text: 'Decrementar',
            ),
          ],
        ),
      ],
    );
  }
}
