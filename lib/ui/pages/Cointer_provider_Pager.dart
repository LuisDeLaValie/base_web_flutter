import 'package:bases_web/providers/Couter_provider.dart';
import 'package:bases_web/ui/shared/Custaom_app_menu.dart';
import 'package:bases_web/ui/shared/Custom_flat_button.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CointerProviderPager extends StatelessWidget {
  const CointerProviderPager({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => CouterProvider('15'),
      child: _PageProvider(),
    );
  }
}

class _PageProvider extends StatelessWidget {
  const _PageProvider({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final pro = Provider.of<CouterProvider>(context);
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          CustaomAppmenu(),
          Spacer(),
          Text(
            "Provider counter",
            style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
            ),
          ),
          FittedBox(
            fit: BoxFit.contain,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: Text(
                "Coutenr: ${pro.couter}",
                style: TextStyle(
                  fontSize: 80,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CustaomFlatButtom(
                onPressed: () => pro.couter++,
                text: 'increment',
              ),
              CustaomFlatButtom(
                onPressed: () => pro.couter--,
                text: 'Decrementar',
              ),
            ],
          ),
          Spacer(),
        ],
      ),
    );
  }
}
