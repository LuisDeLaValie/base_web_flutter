import 'package:bases_web/ui/shared/Custaom_app_menu.dart';
import 'package:bases_web/ui/shared/Custom_flat_button.dart';
import 'package:flutter/material.dart';

class CointerPager extends StatefulWidget {
  const CointerPager({Key? key}) : super(key: key);

  @override
  _CointerPagerState createState() => _CointerPagerState();
}

class _CointerPagerState extends State<CointerPager> {
  int couter = 10;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          CustaomAppmenu(),
          Spacer(),
          Text(
            "Statefull counter",
            style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
            ),
          ),
          FittedBox(
            fit: BoxFit.contain,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: Text(
                "Coutenr: $couter",
                style: TextStyle(
                  fontSize: 80,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CustaomFlatButtom(
                onPressed: () {
                  setState(() {
                    couter++;
                  });
                },
                text: 'increment',
              ),
              CustaomFlatButtom(
                onPressed: () {
                  setState(() {
                    couter--;
                  });
                },
                text: 'Decrementar',
              ),
            ],
          ),
          Spacer(),
        ],
      ),
    );
  }
}
