import 'package:bases_web/services/navigation_services.dart';
import 'package:bases_web/ui/shared/Custom_flat_button.dart';
import 'package:flutter/material.dart';

class CustaomAppmenu extends StatelessWidget {
  const CustaomAppmenu({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (_, BoxConstraints constraints) {
        return (constraints.maxWidth > 520) ? _WebDesk() : _Movile();
      },
    );
  }
}

class _WebDesk extends StatelessWidget {
  const _WebDesk({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      child: Row(
        children: [
          CustaomFlatButtom(
            // onPressed: () => Navigator.pushNamed(context, '/stateful'),
            onPressed: () => navigationServices.navigateTo('/stateful'),
            text: 'Couter Statful',
            color: Colors.black,
          ),
          SizedBox(
            width: 10,
          ),
          CustaomFlatButtom(
            onPressed: () => navigationServices.navigateTo('/provider'),
            text: 'Couter Provider',
            color: Colors.black,
          ),
          SizedBox(
            width: 10,
          ),
          CustaomFlatButtom(
            onPressed: () => navigationServices.navigateTo('abs123'),
            text: 'Otra Pagina',
            color: Colors.black,
          ),
          CustaomFlatButtom(
            onPressed: () => navigationServices.navigateTo('/stateful/100'),
            text: 'Couter Statful 100',
            color: Colors.black,
          ),
          CustaomFlatButtom(
            onPressed: () => navigationServices.navigateTo('/provider?q=200'),
            text: 'Couter Provider 200',
            color: Colors.black,
          ),
        ],
      ),
    );
  }
}

class _Movile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      child: Column(
        children: [
          CustaomFlatButtom(
            // onPressed: () => Navigator.pushNamed(context, '/stateful'),
            onPressed: () => navigationServices.navigateTo('/stateful'),
            text: 'Couter Statful',
            color: Colors.black,
          ),
          SizedBox(
            width: 10,
          ),
          CustaomFlatButtom(
            onPressed: () => navigationServices.navigateTo('/provider'),
            text: 'Couter Provider',
            color: Colors.black,
          ),
          SizedBox(
            width: 10,
          ),
          CustaomFlatButtom(
            onPressed: () => navigationServices.navigateTo('abs123'),
            text: 'Otra Pagina',
            color: Colors.black,
          ),
          CustaomFlatButtom(
            onPressed: () => navigationServices.navigateTo('/stateful/100'),
            text: 'Couter Statful 100',
            color: Colors.black,
          ),
        ],
      ),
    );
  }
}
