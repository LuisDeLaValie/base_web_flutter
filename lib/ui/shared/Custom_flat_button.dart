import 'package:flutter/material.dart';

class CustaomFlatButtom extends StatelessWidget {
  final void Function() onPressed;
  final String text;
  final Color? color;
  const CustaomFlatButtom({
    Key? key,
    required this.onPressed,
    required this.text,
    this.color = Colors.pink,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextButton(
      style: TextButton.styleFrom(
        primary: this.color,
      ),
      onPressed: () => this.onPressed(),
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Text(this.text),
      ),
    );
  }
}
